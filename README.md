![Slack + Bamboo](https://cdn.appstorm.net/web.appstorm.net/web/files/2013/10/slack_icon.png)

Slack Notifications for Bamboo
==============================

This plugin sends [Bamboo](https://www.atlassian.com/software/bamboo) notifications to [Slack](https://www.slack.com/).

You can get it [here](https://marketplace.atlassian.com/plugins/com.atlassian.bamboo.plugins.bamboo-slack)

It allows any Bamboo notification to be posted to a specific slack room.

This plugin allows you to take advantage of Bamboo's:

-	flexible notification system (ie tell me when this build fails more than 5 times!)
-	commenting (comments show up in the chatroom so everyone can see someone's working on the build)

Notifications Supported
-----------------------

-	Build successful
-	Build failed
-	Build commented
-	Job hung
-	Job queue timeout

Setup
-----

1.	Go to the *Notifications* tab of the *Configure Plan* screen.
2.	Choose a *Recipient Type* of *Slack*
3.	Configure your *Slack Web hook URL* : generate a new one [here](https://<yourdomain>.slack.com/services/new/incoming-webhook)
4.	Configure your *Channel Name* (to post message to):
	-	if not configured, will post to the channel configured by the web-hook,
	-	to post to a different room, give the room name, for example #my-room (prefix by #),
	-	to post to a specific user, give the user name, for example @user (prefix by @).
5.	You're done! Go and get building.

Compiling from source
---------------------

You first need to [Set up the Atlassian Plugin SDK](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project). Or you could just do a `brew tap atlassian/tap; brew install atlassian/tap/atlassian-plugin-sdk` on a mac is you use HomeBrew... At the project top level (where the pom.xml is) :

1.	Compile : `atlas-mvn compile`
2.	Run : `atlas-run`
3.	Debug : `atlas-debug`

Feedback? Questions?
--------------------

Twitter: @0x4d4d or mathieu.marache@cstb.fr.
